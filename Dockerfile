FROM node:latest
ADD . /app
WORKDIR /app

RUN npm ci --only=production

EXPOSE 3000
CMD [ "node", "start" ]
